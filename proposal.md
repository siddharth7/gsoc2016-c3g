# GSoC 2016 proposal: MUGQIC
## Project Info:
### Project Title: 
EGA Data Submission database (mEGAdata)
### Project Short title: 
Develop an open-source light weight web application using flask and angular js to assist any genomics project to submit their data to the EGA( European Genome-phenome Archive).
### URL of project idea page:
https://bitbucket.org/mugqic/gsoc2016/overview#markdown-header-ega-data-submission-database-megadata

## Biographical Information:
My name is Siddharth Singh, I'm 20 years old, 2nd year undergraduate student studying Computer Science Engineering(B.Tech) at Indraprastha Institute of Information Technology(IIIT), Delhi, India. I am expected to graduate in 2018.
I have a strong background in Python, C, C++, Java. I have an experience of full stack development of a production level website(http://tagtraqr.com) in python and several hackathon projects build overnight. I am confident and motivated that I will be able to complete this particular project in time successfully.
Github link : https://github.com/siddharth7/
CV link : https://drive.google.com/file/d/0BwpDsVa-QXwgTkU0ZURfNU1mT1E/view?usp=sharing

## Contact Information:
- Institution: Indraprastha Institute of Information Technology, New Delhi, India
- Program: Bachelors in Computer Science Engineering
- Stage of completion: 2nd year(Expected graduation: 2018)
- Contact to verify: sheetu@iiitd.ac.in (Manager, Academics)

## Schedule Conflicts
No, I don't have any other commitments for the summer.

## Mentors
- Mentor names: David Bujold
- Mentor emails: david.bujold@computationalgenomics.ca
- Mentor link_ids: https://bitbucket.org/dbujold/
- Have you been in touch with the mentors? When and how? : Yes, I have been in touch with the mentor via Emails since 3rd March.

## Student Affiliation
- Institution: Indraprastha Institute of Information Technology, Delhi
- Program: Bachelors in Engineering, Computer Science
- Stage of Completion: Currently in 2nd year
- Contact to verify: sheetu@iiitd.ac.in

## Synopsis
- mEGAdata is a light-weight flask application to ease out the process of submission and archiving of genomics data to the EGA website. I am listing some points below that needs to be worked upon in the project:
  - The submission of these large datasets that account to about Terabytes in size needs to be transferred safely and accurately to EGA website in a more automated and user friendly way. This could be achieved by using a FTP, but the transfer speed would ve very slow, another solution is to use Aspera, a High Speed file transfer SAAS solution. Aspera can be integrated in the project with the help of Aspera SDKs available with Aspera. This will enable one-touch, secure and fast implementation of data transfers. I read more about it from here (https://www.ebi.ac.uk/ega/submission/tools/FTP_aspera)
  - Since the EGA web interface is not tailored for submitting a lot of samples in one go, the data needs to be split into chunks and then send with the help of scripts that would automate the submission process. I read this link (https://www.ebi.ac.uk/ega/submission/tools/EGA_webin) to try to understand more about submission of data to EGA.
  -  Developing new components: As clarified with the mentor, a component, basically a sub  web-app needs to be created with a database support. Since the information has to be private with human, the data needs to be hashed and stored in database till it is transferred to EGA. The database entry would actually act as metadata to the result of the sequence run.
  -  To make this application open source and easy to use, the application needs to be integrated with Docker. Docker give the freedom to define environments, and create and deploy apps faster and easier with continuous deployment. This easy distribution will ensure high number of users. I would suggest here to use Buddy (https://buddy.works) to build, test and deploy the web app in an efficient way. It is compatible with all the technologies like Github, bitbucket, amazon S3, heruku and python.

## Benefits to Community
The development team of C3G community is a group of experienced developers who have been building robust genomics data analysis pipeline set and working closely with the researchers in the particular field. I will ensure that user base  contribution in the development of mEGAdata maintains the high standards of the community. My work would ensure that the researcher are able to process and send the genomics data securely and error free to the EGA controlled repository. A user-friendly UI will ensure that researchers don't waste their time in figuring out how and what to do and focus on their main part of studying and analysing genomics data. The main goal of the project will be to automate things as far as possible to make sure that application is user friendly. This will come as a handy tool for researchers to submit data to EGA and give them statistics regarding the data too. The docker application will provide an easy and fast implementation of the application for any organisation. This will help the community in increasing their user base in the genomics community.

## Coding Plan & Methods
- #### Assays metadata management
  - Developing a new endpoint/page containing a GET,POST,DELETE to manage updation queries for the metadata collection. 
  
  - Add data dependencies checks to linked database entries so that updation/ deletion doesn't causes any error in dataset analysis further. Data dependencies will be found by studying the functional dependencies among the database tables. This will let us know if the functional dependencies are in 1NF, 2NF, 3NF OR BC-NF. This will let us know the proper image of dependencies, we can also alter the tables once we get the functional dependencies to reduce clashes due to dependencies.
- #### Sequencing runs metadata management
  - User interface for dataset: This will include creating a new page using angular js and with the help of rest apis in flask, the appropriate data can be put up on the page. Since these raw files are written in FASTQ format, a python scrapper can we written to segregate data, if needed, from the metadata file or the FASTQ format file. Since FASTQ is a text based format and it follows a particular schema of detail of each sequence in 4 lines where these lines contain sequence identifier, raw letters, a plus sign, quality value respectively, scrapping and showing data won't be a difficult task.
  
  - Similarly, server location and md5 checksums from metadata file can be scrapped and displayed properly.
- #### Bioinformatics downstream analysis metadata
  - New endpoint creation as above. A new database schema and front-end needs to be developed

- #### Donors, sample properties editors from the web application
  - Metadata information needs to be edited here, a new endpoint and front-end would be developed for the same.

- #### Global application search feature
  - the direct way to do that would be to use sql command for it, for example
`SELECT id, category, location
FROM table
WHERE
(
    category LIKE '%keyword%'
    OR location LIKE '%keyword%'
)
`
  - I am still searching if it could be implemented faster that the above but I don't think it would be possible because these days the SQL data is stored in a B+ tree which gives a ` O(logn)` search complexity on a single table without using join which is pretty good as compared to a `O(n)` complexity.

- #### XML generation
  - Two things can be done here, either port the perl script to python.
  - Or use inbuilt python packages like elementTree(https://docs.python.org/2/library/xml.etree.elementtree.html) or lxml(http://lxml.de/tutorial.html#the-e-factory). This would provide us will all OOPS features, reusability and clean structure.

- #### BioPortal ontologies integration
  - Integrating Handsontable: I haven't used it personally, but after seeing the docs, I can implement that on the front end without a issue. The community bonding period can be used to learn more about it, if required.
 
- #### Application authentication
    - Google Oauth2 would be better to use as it has a large coverage and a user base, thereby not limiting us with the issue of selected people using google services. I have had implemented it for this website (http://tagtraqr.com). I implemented it on Django framework, but the python packages aren't different for Flask. I have used this package in my implementation (http://psa.matiasaguirre.net/docs/index.html). It provides decorator functions like `@login_required` for a endpoint and can be integrated in the ORM(PeeWee) too. It's just like any other Oauth2 implementation with a key and secret generated from a particular system. You also need to provide the SHA key while generating them from the Google Developer Console.
    - Any other Oauth2 integration could also be thought upon, two different authentication backends will be handled by this python package accurately. It even provides an option to merge two particular contacts with different backends sort of things.

- #### Application portability
    - The question here is basically docker or a VM. After reading about both of them, I am considering docker a better option for such kind of application.
    - Reason for Docker: Docker is a powerful solution when packing/shipping portable software. Deployment speed: Docker maintains the minimum requirements for running an application. Since this project is basically a light-weight database application, docker will be able to able to handle it pretty well. Portability: Since docker applications are self sufficient application bundles, cross compilation shouldn’t be a problem. Versioned: These docker application can be versioned and can be easily rolled back too. They provide all functionalities like a version control repo.
  - Reason for VM: VM is useful when you have IaaS (Infrastructure as a Service) to work and build upon that has multiple guest OSs and a intensive hardware virtualisation. VMs are often larger in size than a docker and their startup time is also higher compared to a docker application. VMs are highly resource intensive
Virtual machines generally loads full OS with its own file system, memory management etc. A perfect thing to use in cloud computing. But our application is meant to be light weight and efficient and I am sure that docker won’t disappoint us in any way.
  
  - Docker implementation can we worked out using Buddy( https://buddy.works ). It provides Automated Delivery Pipeline and Cross Platform Support for any project. This will help us in making a perfect docker container for our application with full git support and continuous deployment methods. 
  
  - Considering the above differences, I would rather chose to go with a docker. Any inputs are welcome.

###### I have mentioned the work in coding plan so I am only listing the pointers in the timeline

## Timeline
#### Community Bonding Period 
Familiarise myself more with the source code and the C3G developer community.
I won’t be available from the last week of April to 3rd-4th May due to University final exams. I would go through handsontable and docker implementations too in this period and visualise the database schema currently developed. I am planning to start coding 10 days ahead of official Coding start date i.e around 14 May.
I will be following code, test, document method where I will code, test it and prepare documentation side by side. Same approach will go for each week's schedule.
##### Tasks for 14 May - 11 June (4 Weeks)
- Assays metadata management
- Sequencing runs metadata management
- Bioinformatics downstream analysis metadata
- Donors, sample properties editors from the web application

During this period, I will pick up one task for roughly each week from the above and code it, unit testing will be done using Karma and Jasmine for AngularJs part and Flask-Testing for flask backend APIs, and document it in the end
Keeping a 10 days buffer period before mid-term evaluation.

My mid term evaluation will cover the work done between 14 May - 11 June.

##### Tasks for 12 June - 23 June (1.5 Weeks)
- Global application search feature

##### Tasks for 24 June - 3 July (1.5 Weeks)
- XML generation

##### Tasks for 4 July - 14 July(1.5 Weeks)
- BioPortal ontologies integration

##### Tasks for 15 July - 22 July(1 Week)
- Application authentication

##### Tasks for 22 July - 6 August(2 Weeks)
- Application portability

##### Tasks for 6 August - 13 August(1 week)
- Code cleanup

Keeping a 10 days buffer period for end term evaluation that begins from 15 August.

## Management of Coding Project
I am proposing a methodology where I will code, test, and document it side by side. I am not waiting till the end to test and document my code.

## Test
The selection test repository is available here (https://github.com/siddharth7/Mugqic-rest-api)
It was submitted on 10th March. I have solved this particular part i.e Making REST APIs in python previously too for this website (http://tagtraqr.com).



